package t12.nubes_magna.sudoku_service.model;


import java.io.Serializable;

public class Board implements Serializable {

    private static final long serialVersionUID = -2608316042354641012L;

    private Cell[][] cells;
    private boolean solved = false;


    public Board(Cell[][] cells) {

        this.cells = cells;
    }

    public Board() {
    }

    public Cell[][] getCells() {
        return cells;
    }

    public void setCells(Cell[][] cells) {
        this.cells = cells;
    }

    public boolean isSolved() {
        return solved;
    }

    public void setSolved(final boolean solved) {
        this.solved = solved;
    }

}
