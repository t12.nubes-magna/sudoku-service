package t12.nubes_magna.sudoku_service.model.rest;


import java.io.Serializable;

public class BoardExternal implements Serializable {

    private static final long serialVersionUID = -6842396896146839026L;

    private int[][] cells;
    private boolean solved = false;


    public BoardExternal(int[][] cells) {

        this.cells = cells;
    }

    public BoardExternal() {
    }

    public int[][] getCells() {
        return cells;
    }

    public void setCells(int[][] cells) {
        this.cells = cells;
    }

    public boolean isSolved() {
        return solved;
    }

    public void setSolved(final boolean solved) {
        this.solved = solved;
    }

}
