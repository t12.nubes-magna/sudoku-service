package t12.nubes_magna.sudoku_service.model.converter;


import org.springframework.core.convert.converter.Converter;
import t12.nubes_magna.sudoku_service.model.rest.BoardExternal;
import t12.nubes_magna.sudoku_service.model.Board;
import t12.nubes_magna.sudoku_service.model.Cell;

public class BoardToBoardExternalConverter implements Converter<Board, BoardExternal> {

    @Override
    public BoardExternal convert(final Board source) {

        final Cell[][] sourceCells = source.getCells();
        final int[][] cells = new int[sourceCells.length][sourceCells[0].length];
        for (int row = 0; row < cells.length; ++row) {
            for (int column = 0; column < cells[row].length; ++column) {
                cells[row][column] = sourceCells[row][column].getValue();
            }
        }

        final BoardExternal boardExternal = new BoardExternal(cells);
        boardExternal.setSolved(source.isSolved());

        return boardExternal;
    }
}
