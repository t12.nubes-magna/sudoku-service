package t12.nubes_magna.sudoku_service.model.converter;


import org.springframework.core.convert.converter.Converter;
import t12.nubes_magna.sudoku_service.model.rest.BoardExternal;
import t12.nubes_magna.sudoku_service.model.Board;
import t12.nubes_magna.sudoku_service.model.Cell;

import java.util.ArrayList;

public class BoardExternalToBoardConverter implements Converter<BoardExternal, Board> {

    @Override
    public Board convert(final BoardExternal source) {

        final int[][] sourceCells = source.getCells();
        final Cell[][] cells = new Cell[sourceCells.length][sourceCells[0].length];
        for (int row = 0; row < cells.length; ++row) {
            for (int column = 0; column < cells[row].length; ++column) {
                cells[row][column] = new Cell(sourceCells[row][column], new ArrayList<>());
            }
        }

        final Board board = new Board(cells);
        board.setSolved(source.isSolved());

        return board;
    }
}
