package t12.nubes_magna.sudoku_service.model;

import java.io.Serializable;
import java.util.List;

public class Cell implements Serializable {

    private static final long serialVersionUID = -431342026245290999L;

    private int value;
    private List<Integer> markup;

    public Cell(int value, List<Integer> markup) {
        this.value = value;
        this.markup = markup;
    }

    public Cell() {
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public List<Integer> getMarkup() {
        return markup;
    }

    public void setMarkup(List<Integer> markup) {
        this.markup = markup;
    }
}
