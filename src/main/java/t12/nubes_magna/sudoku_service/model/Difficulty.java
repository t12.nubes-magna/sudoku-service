package t12.nubes_magna.sudoku_service.model;

import com.fasterxml.jackson.annotation.JsonFormat;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum Difficulty {

    SUPER_EASY(50, 80, 1), EASY(36, 49, 2), MEDIUM(32, 35, 3), DIFFICULT(28, 31, 4), EVIL(22, 27, 5), SUPER_EVIL(16, 21,
            6);


    private String level = this.name();
    private int minGivens;
    private int maxGivens;
    private int score;

    Difficulty(final int minGivens, final int maxGivens, final int score) {
        this.minGivens = minGivens;
        this.maxGivens = maxGivens;
        this.score = score;
    }

    public int getMinGivens() {
        return minGivens;
    }

    public int getMaxGivens() {
        return maxGivens;
    }

    public int getScore() {
        return score;
    }

    public String getLevel() {
        return level;
    }
}
