package t12.nubes_magna.sudoku_service.service;

import org.springframework.stereotype.Service;
import t12.nubes_magna.sudoku_service.model.Board;
import t12.nubes_magna.sudoku_service.model.Cell;

import java.util.ArrayList;


@Service
public class SudokuSolvingServiceImpl implements SudokuSolvingService {

    public static final int CELLS_NUMBER = 9;

    private SudokuUtility sudokuUtility = new SudokuUtility();

    @Override
    public Board stringRepresentationToBoardObject(final String representation) {

        Cell[][] cells = new Cell[SudokuSolvingServiceImpl.CELLS_NUMBER][SudokuSolvingServiceImpl.CELLS_NUMBER];
        final String s = representation.replace(",", "").replaceAll("\\s+", "");

        int index = 0;
        for (int row = 0; row < cells.length; ++row) {
            for (int column = 0; column < cells[row].length; ++column) {
                cells[row][column] = new Cell(Character.getNumericValue(s.charAt(index)), new ArrayList<>());
                ++index;
            }
        }
        final Board board = new Board(cells);
        return board;
    }

    @Override
    public boolean checkIfSolutionIsCorrect(final Board board) {
        if (checkIfCompleted(board)) {
            return validateSolution(board);
        } else {
            return false;
        }
    }

    private boolean validateSolution(final Board board) {
        for (int row = 0; row < board.getCells().length; ++row) {
            if (!checkIValidSumOfCells(sudokuUtility.extractRow(board.getCells(), row))) {
                return false;
            }
        }

        for (int column = 0; column < board.getCells()[0].length; ++column) {
            if (!checkIValidSumOfCells(sudokuUtility.extractColumns(board.getCells(), column))) {
                return false;
            }
        }

        for (int row = 0; row < board.getCells().length; ++row) {
            if (sudokuUtility.findDuplicatesInCells(sudokuUtility.extractRow(board.getCells(), row))) {
                return false;
            }
        }

        for (int column = 0; column < board.getCells()[0].length; ++column) {
            if (sudokuUtility.findDuplicatesInCells(sudokuUtility.extractColumns(board.getCells(), column))) {
                return false;
            }
        }

        for (int row = 0; row < board.getCells().length; ++row) {
            for (int column = 0; column < board.getCells()[row].length; ++column) {
                if (!checkIValidSumOfCells(sudokuUtility.extractMiniGrid(board.getCells(), row, column))) {
                    return false;
                }
            }
        }

        for (int row = 0; row < board.getCells().length; ++row) {
            for (int column = 0; column < board.getCells()[row].length; ++column) {
                if (sudokuUtility.findDuplicatesInCells(sudokuUtility.extractMiniGrid(board.getCells(), row, column))) {
                    return false;
                }
            }
        }
        return true;
    }


    private boolean checkIValidSumOfCells(final Cell[] cells) {
        int sum = 0;
        for (Cell cell : cells) {
            sum += cell.getValue();
        }
        return sum == (CELLS_NUMBER * (CELLS_NUMBER + 1) / 2);
    }

    @Override
    public Board solve(final Board originalBoard) {

        final Board board = prepareMarkup(originalBoard);

        smartSolve(board);

        if (checkIfSolutionIsCorrect(board)) {
            board.setSolved(true);
            return board;
        }


        return backtrackingBruteForce(board);
    }

    private Board backtrackingBruteForce(final Board board) {

        final boolean solved = backtrackingBruteForceSolve(board.getCells(), 0, 0);
        board.setSolved(solved);
        return board;
    }

    private boolean backtrackingBruteForceSolve(final Cell[][] cells, int row, int column) {

        if (column >= cells[0].length) {
            column = 0;
            ++row;
        }
        if (row >= cells.length) {
            return true;
        }

        if (cells[row][column].getValue() != 0) {
            return backtrackingBruteForceSolve(cells, row, column + 1);
        }

        for (int number = 1; number <= 9; number++) {

            if (!sudokuUtility.isCellValid(cells, row, column, number)) {
                cells[row][column].setValue(0);
                continue;
            }
            cells[row][column].setValue(number);

            if (backtrackingBruteForceSolve(cells, row, column + 1)) {
                cells[row][column].getMarkup().clear();
                cells[row][column].getMarkup().add(number);
                return true;
            } else {
                cells[row][column].setValue(0);
            }

        }

        return false;
    }

    private Board prepareMarkup(final Board originalBoard) {
        for (int row = 0; row < originalBoard.getCells().length; ++row) {
            for (int column = 0; column < originalBoard.getCells()[row].length; ++column) {
                final Cell cell = originalBoard.getCells()[row][column];

                if (cell.getValue() == 0) {
                    for (int number = 1; number <= 9; ++number) {
                        cell.getMarkup().add(number);
                    }
                } else {
                    cell.getMarkup().add(cell.getValue());
                }
            }
        }
        return originalBoard;
    }

    private boolean checkIfCompleted(final Board board) {

        for (int row = 0; row < board.getCells().length; ++row) {
            for (int column = 0; column < board.getCells()[row].length; ++column) {
                if (board.getCells()[row][column].getValue() == 0) {
                    return false;
                }
            }
        }
        return true;
    }

    private Board smartSolve(final Board board) {

        supplementMarkup(board);
        while (true) {
            if (!findHiddenSingle(board)) {
                break;
            }
        }

        return board;
    }

    private boolean findHiddenSingle(final Board board) {
        boolean foundHiddenSingle = false;
        if (findHiddenSingleInRow(board) || findHiddenSingleInColumn(board)) {
            foundHiddenSingle = true;
        }
        return foundHiddenSingle;
    }

    private boolean findHiddenSingleInColumn(final Board board) {
        boolean foundHiddenSingle = false;
        for (int row = 0; row < board.getCells().length; ++row) {
            for (int column = 0; column < board.getCells()[row].length; ++column) {
                final Cell targetedCell = board.getCells()[row][column];
                if (targetedCell.getValue() == 0) {

                    final Cell[] cells = sudokuUtility.extractColumns(board.getCells(), column);
                    ArrayList<Integer> rowMarkup = new ArrayList<>();
                    for (Cell cell : cells) {
                        if (cell.getValue() == 0) {
                            rowMarkup.addAll(cell.getMarkup());
                        }
                    }
                    rowMarkup.removeAll(sudokuUtility.findDuplicates(rowMarkup));

                    foundHiddenSingle = revealHiddenSingle(foundHiddenSingle, cells, rowMarkup);

                    supplementMarkup(board);
                }
            }
        }
        return foundHiddenSingle;
    }

    private boolean revealHiddenSingle(boolean foundHiddenSingle, final Cell[] cells, final ArrayList<Integer> rowMarkup) {
        if (!rowMarkup.isEmpty()) {
            for (Integer markup : rowMarkup) {
                for (Cell cell : cells) {
                    if (cell.getMarkup().contains(markup)) {
                        cell.getMarkup().clear();
                        cell.getMarkup().add(markup);
                        cell.setValue(markup);
                        foundHiddenSingle = true;
                    }
                }
            }
        }
        return foundHiddenSingle;
    }

    private boolean findHiddenSingleInRow(final Board board) {
        boolean foundHiddenSingle = false;
        for (int row = 0; row < board.getCells().length; ++row) {
            for (int column = 0; column < board.getCells()[row].length; ++column) {
                final Cell targetedCell = board.getCells()[row][column];
                if (targetedCell.getValue() == 0) {

                    final Cell[] cells = sudokuUtility.extractRow(board.getCells(), row);
                    ArrayList<Integer> rowMarkup = new ArrayList<>();
                    for (Cell cell : cells) {
                        if (cell.getValue() == 0) {
                            rowMarkup.addAll(cell.getMarkup());
                        }
                    }
                    rowMarkup.removeAll(sudokuUtility.findDuplicates(rowMarkup));

                    foundHiddenSingle = revealHiddenSingle(foundHiddenSingle, cells, rowMarkup);
                }
            }
        }
        return foundHiddenSingle;
    }

    private void supplementMarkup(final Board board) {
        for (int row = 0; row < board.getCells().length; ++row) {
            for (int column = 0; column < board.getCells()[row].length; ++column) {
                final Cell cell = board.getCells()[row][column];

                if (cell.getValue() == 0) {
                    for (int number = 1; number <= 9; ++number) {

                        if (sudokuUtility.isNumberInCells(sudokuUtility.extractRow(board.getCells(), row), number) ||
                                sudokuUtility.isNumberInCells(sudokuUtility.extractColumns(board.getCells(), column), number) ||
                                sudokuUtility.isNumberInCells(sudokuUtility.extractMiniGrid(board.getCells(), row, column), number)) {
                            cell.getMarkup().remove(Integer.valueOf(number));
                        }

                        if (number == 9 && cell.getMarkup().size() == 1) {
                            cell.setValue(cell.getMarkup().get(0));
                            supplementMarkup(board);
                        }
                    }
                }
            }
        }
    }


}
