package t12.nubes_magna.sudoku_service.service;

import t12.nubes_magna.sudoku_service.model.Board;
import t12.nubes_magna.sudoku_service.model.Difficulty;

public interface SudokuGenerationService {

    Board generateBoard(Difficulty difficulty);
}
