package t12.nubes_magna.sudoku_service.service;


import t12.nubes_magna.sudoku_service.model.Cell;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

public class SudokuUtility {

    public SudokuUtility() {
    }

    public boolean isCellValid(final Cell[][] cells, final int row, final int column, final int number) {
        if (findNumberInCells(extractRow(cells, row), number)) {
            return false;
        }
        if (findNumberInCells(extractColumns(cells, column), number)) {
            return false;
        }
        if (findNumberInCells(extractMiniGrid(cells, row, column), number)) {
            return false;
        }

        return true;
    }

    private boolean findNumberInCells(final Cell[] cells, final int number) {
        for (Cell cell : cells) {
            if (cell.getValue() == number) {
                return true;
            }
        }
        return false;
    }

    public Set<Integer> findDuplicates(List<Integer> listContainingDuplicates) {

        final Set<Integer> setToReturn = new HashSet<>();
        final Set<Integer> set1 = new HashSet<>();

        for (Integer yourInt : listContainingDuplicates) {
            if (yourInt !=0 && !set1.add(yourInt)) {
                setToReturn.add(yourInt);
            }
        }
        return setToReturn;
    }

    public boolean findDuplicatesInCells(final Cell[] cells) {
        final List<Integer> values = new ArrayList<>();
        for (Cell cell : cells) {
            values.add(cell.getValue());
        }

        return !findDuplicates(values).isEmpty();
    }

    public boolean isNumberInCells(final Cell[] row, final int number) {
        for (int i = 0; i < row.length; ++i) {
            if (row[i].getValue() == number) {
                return true;
            }
        }
        return false;
    }


    public Cell[] extractRow(final Cell[][] cells, final int rowNumber) {
        return Arrays.copyOfRange(cells[rowNumber], 0, cells[rowNumber].length);
    }

    public Cell[] extractColumns(final Cell[][] cells, final int columnNumber) {
        final Cell[] column = new Cell[cells.length];
        for (int row = 0; row < cells.length; ++row) {
            column[row] = cells[row][columnNumber];
        }
        return column;
    }

    public Cell[] extractMiniGrid(final Cell[][] cells, final int row, final int column) {

        final List<Cell> gridCellsList = new ArrayList<>();

        int gridStartRow = getGridStartPoint(row);
        int gridStartColumn = getGridStartPoint(column);


        for (int i = gridStartRow; i < gridStartRow + 3; ++i) {
            for (int j = gridStartColumn; j < gridStartColumn + 3; ++j) {
                gridCellsList.add(cells[i][j]);
            }
        }


        return gridCellsList.toArray(new Cell[0]);
    }

    private int getGridStartPoint(final int pointInGrid) {
        return 3 * (pointInGrid / 3);
    }

    public ArrayList<Integer> prepareNumbersInRandomOrder() {

        final Random random = new Random();
        final ArrayList<Integer> list = new ArrayList<>();
        for (int number = 1; number <= 9; number++) {
            final int randomNumber = random.nextInt(9) + 1;
            if (!list.contains(randomNumber)) {
                list.add(randomNumber);
            }
        }
        return list;
    }
}
