package t12.nubes_magna.sudoku_service.service;


import t12.nubes_magna.sudoku_service.model.Board;

public interface SudokuSolvingService {

    Board solve(final Board board);

    Board stringRepresentationToBoardObject(final String representation);

    boolean checkIfSolutionIsCorrect(final Board board);
}
