package t12.nubes_magna.sudoku_service.service;


import org.springframework.stereotype.Service;
import t12.nubes_magna.sudoku_service.model.Board;
import t12.nubes_magna.sudoku_service.model.Difficulty;
import t12.nubes_magna.sudoku_service.model.Cell;

import java.util.ArrayList;
import java.util.Random;

@Service
public class SudokuGenerationServiceImpl implements SudokuGenerationService {

    private SudokuUtility sudokuUtility = new SudokuUtility();

    @Override
    public Board generateBoard(final Difficulty difficulty) {

        final Board board = prepareEmptyBoard();
        putRandomNumberInRandomCell(board.getCells());

        supplementBoardWithNumbers(board.getCells(), 0, 0);
        removeNumbersFromBoard(board.getCells(), difficulty);

        return board;
    }

    private void removeNumbersFromBoard(final Cell[][] cells, final Difficulty difficulty) {
        final Random random = new Random();

        final int numberOfGivens = random
                .nextInt(difficulty.getMaxGivens() - difficulty.getMinGivens() + 1) + difficulty.getMinGivens();
        final int cellsToErase = (SudokuSolvingServiceImpl.CELLS_NUMBER * SudokuSolvingServiceImpl.CELLS_NUMBER) - numberOfGivens;

        int cellsErased = 0;

        while (cellsErased < cellsToErase) {
            final int row = random.nextInt(9);
            final int column = random.nextInt(9);

            final Cell cellToErase = cells[row][column];
            if (cellToErase.getValue() != 0) {
                cellToErase.setValue(0);
                cellToErase.getMarkup().clear();
                ++cellsErased;
            }
        }
    }

    private boolean supplementBoardWithNumbers(final Cell[][] cells, int row, int column) {

        if (column >= cells[0].length) {
            column = 0;
            ++row;
        }
        if (row >= cells.length) {
            return true;
        }

        if (cells[row][column].getValue() != 0) {
            return supplementBoardWithNumbers(cells, row, column + 1);
        }

        ArrayList<Integer> arrayDeque = sudokuUtility.prepareNumbersInRandomOrder();

        for (Integer number : arrayDeque) {

            if (!sudokuUtility.isCellValid(cells, row, column, number)) {
                cells[row][column].setValue(0);
                continue;
            }
            cells[row][column].setValue(number);

            if (supplementBoardWithNumbers(cells, row, column + 1)) {
                cells[row][column].getMarkup().clear();
                cells[row][column].getMarkup().add(number);
                return true;
            } else {
                cells[row][column].setValue(0);
            }

        }
        return false;
    }


    private void putRandomNumberInRandomCell(final Cell[][] cells) {
        final Random random = new Random();
        final int randomRow = random.nextInt(cells.length);
        final int randomColumn = random.nextInt(cells[0].length);
        final int randomNumber = random.nextInt(9) + 1;
        cells[randomRow][randomColumn].setValue(randomNumber);
        cells[randomRow][randomColumn].getMarkup().add(randomNumber);
    }

    private Board prepareEmptyBoard() {

        Cell[][] cells = new Cell[SudokuSolvingServiceImpl.CELLS_NUMBER][SudokuSolvingServiceImpl.CELLS_NUMBER];
        for (int row = 0; row < cells.length; ++row) {
            for (int column = 0; column < cells.length; ++column) {
                cells[row][column] = new Cell(0, new ArrayList<>());
            }
        }

        final Board board = new Board(cells);
        return board;
    }
}
