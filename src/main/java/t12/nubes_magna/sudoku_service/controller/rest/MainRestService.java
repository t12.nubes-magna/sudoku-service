package t12.nubes_magna.sudoku_service.controller.rest;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import t12.nubes_magna.sudoku_service.model.Difficulty;
import t12.nubes_magna.sudoku_service.model.rest.BoardExternal;

@RequestMapping("/sudoku")
public interface MainRestService {

    @GetMapping(value = "/generate")
    BoardExternal generate(@RequestParam(name = "difficulty", defaultValue = "MEDIUM") String difficultyLevel);

    @PostMapping(value = "/solve", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    BoardExternal solve(@RequestBody BoardExternal boardExternal);

    @PostMapping(value = "/solve", consumes = MediaType.TEXT_PLAIN_VALUE)
    BoardExternal solveStringRepresentation(@RequestBody String stringRepresentation);

    @PostMapping(value = "/convertFromString", consumes = MediaType.TEXT_PLAIN_VALUE)
    BoardExternal stringRepresentationToBoardObject(@RequestBody String stringRepresentation);

    @PostMapping(value = "/checkSolution", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    boolean checkSolution(@RequestBody BoardExternal boardExternal);

    @PostMapping(value = "/checkSolution", consumes = MediaType.TEXT_PLAIN_VALUE)
    boolean checkSolutionFromStringRepresentation(@RequestBody String stringRepresentation);

    @GetMapping(value = "/difficulty")
    Difficulty[] difficultyLevels();
}
