package t12.nubes_magna.sudoku_service.controller.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import t12.nubes_magna.sudoku_service.model.Difficulty;
import t12.nubes_magna.sudoku_service.model.converter.BoardExternalToBoardConverter;
import t12.nubes_magna.sudoku_service.model.converter.BoardToBoardExternalConverter;
import t12.nubes_magna.sudoku_service.model.rest.BoardExternal;
import t12.nubes_magna.sudoku_service.service.SudokuSolvingService;
import t12.nubes_magna.sudoku_service.service.SudokuGenerationService;

@RestController
public class MainRestServiceImpl implements MainRestService {

    @Autowired
    private SudokuSolvingService sudokuSolvingServiceImpl;
    @Autowired
    private SudokuGenerationService sudokuGenerationServiceImpl;

    private BoardToBoardExternalConverter boardToBoardExternalConverter = new BoardToBoardExternalConverter();
    private BoardExternalToBoardConverter boardExternalToBoardConverter = new BoardExternalToBoardConverter();

    @Override
    public BoardExternal generate(@RequestParam(name = "difficulty", defaultValue = "MEDIUM") String difficultyLevel) {
        Difficulty difficulty;
        try {
            difficulty = Difficulty.valueOf(difficultyLevel);
        } catch (IllegalArgumentException e) {
            difficulty = Difficulty.MEDIUM;
        }
        return boardToBoardExternalConverter.convert(sudokuGenerationServiceImpl.generateBoard(difficulty));
    }

    @Override
    public BoardExternal solve(@RequestBody BoardExternal boardExternal) {
        return boardToBoardExternalConverter
                .convert(sudokuSolvingServiceImpl.solve(boardExternalToBoardConverter.convert(boardExternal)));
    }

    @Override
    public BoardExternal solveStringRepresentation(@RequestBody String stringRepresentation) {
        return boardToBoardExternalConverter.convert(sudokuSolvingServiceImpl
                .solve(sudokuSolvingServiceImpl.stringRepresentationToBoardObject(stringRepresentation)));
    }

    @Override
    public BoardExternal stringRepresentationToBoardObject(@RequestBody String stringRepresentation) {
        return boardToBoardExternalConverter
                .convert(sudokuSolvingServiceImpl.stringRepresentationToBoardObject(stringRepresentation));
    }

    @Override
    public boolean checkSolution(@RequestBody BoardExternal boardExternal) {
        return sudokuSolvingServiceImpl.checkIfSolutionIsCorrect(boardExternalToBoardConverter.convert(boardExternal));
    }

    @Override
    public boolean checkSolutionFromStringRepresentation(@RequestBody String stringRepresentation) {
        return sudokuSolvingServiceImpl.checkIfSolutionIsCorrect(
                sudokuSolvingServiceImpl.stringRepresentationToBoardObject(stringRepresentation));
    }

    @Override
    public Difficulty[] difficultyLevels() {
        return Difficulty.values();
    }
}
