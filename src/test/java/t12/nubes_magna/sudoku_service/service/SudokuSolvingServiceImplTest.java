package t12.nubes_magna.sudoku_service.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import t12.nubes_magna.sudoku_service.model.Board;
import t12.nubes_magna.sudoku_service.service.SudokuSolvingService;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

@SpringBootTest
public class SudokuSolvingServiceImplTest extends AbstractTestNGSpringContextTests {

    @Autowired
    private SudokuSolvingService sudokuSolvingServiceImpl;

    @Test
    public void testCheckSolutionFail() {

        Assert.assertEquals(sudokuSolvingServiceImpl.checkIfSolutionIsCorrect(sudokuSolvingServiceImpl.stringRepresentationToBoardObject(
                new StringBuilder()
                        .append("2,2,3,4,5,6,7,8,9,")
                        .append("4,5,6,7,8,9,1,2,3,")
                        .append("7,8,9,1,2,3,4,5,6,")
                        .append("2,3,4,5,6,7,8,9,1,")
                        .append("5,6,7,8,9,1,2,3,4,")
                        .append("8,9,1,2,3,4,5,6,7,")
                        .append("3,4,5,6,7,8,9,1,2,")
                        .append("6,7,8,9,1,2,6,4,5,")
                        .append("9,1,2,3,4,5,6,7,8").toString())), false);
    }

    @Test
    public void testCheckSolutionSuccess() {
        Assert.assertEquals(sudokuSolvingServiceImpl.checkIfSolutionIsCorrect(sudokuSolvingServiceImpl.stringRepresentationToBoardObject(
                new StringBuilder()
                        .append("1,2,3,4,5,6,7,8,9,")
                        .append("4,5,6,7,8,9,1,2,3,")
                        .append("7,8,9,1,2,3,4,5,6,")
                        .append("2,3,4,5,6,7,8,9,1,")
                        .append("5,6,7,8,9,1,2,3,4,")
                        .append("8,9,1,2,3,4,5,6,7,")
                        .append("3,4,5,6,7,8,9,1,2,")
                        .append("6,7,8,9,1,2,3,4,5,")
                        .append("9,1,2,3,4,5,6,7,8").toString())), true);

    }

    @Test(dataProvider = "testBoards")
    public void testSolve(final Board board) {

        final Board solvedBoard = sudokuSolvingServiceImpl.solve(board);

        Assert.assertEquals(sudokuSolvingServiceImpl.checkIfSolutionIsCorrect(solvedBoard), true);
    }


    @DataProvider(name = "testBoards")
    private Object[][] createBoardsForTests() {

        return new Object[][]{
                {sudokuSolvingServiceImpl.stringRepresentationToBoardObject(
                        new StringBuilder()
                                .append("0,8,0,4,0,2,0,6,0,")
                                .append("0,3,4,0,0,0,9,1,0,")
                                .append("9,6,0,0,0,0,0,8,4,")
                                .append("0,0,0,2,1,6,0,0,0,")
                                .append("0,0,0,0,0,0,0,0,0,")
                                .append("0,0,0,3,5,7,0,0,0,")
                                .append("8,4,0,0,0,0,0,7,5,")
                                .append("0,2,6,0,0,0,1,3,0,")
                                .append("0,9,0,7,0,1,0,4,0").toString())},
                {sudokuSolvingServiceImpl.stringRepresentationToBoardObject(
                        new StringBuilder()
                                .append("0,3,9,5,0,0,0,0,0,")
                                .append("0,0,0,8,0,0,0,7,0,")
                                .append("0,0,0,0,1,0,9,0,4,")
                                .append("0,0,0,4,0,0,0,0,3,")
                                .append("0,0,0,0,0,0,0,0,0,")
                                .append("0,0,7,0,0,0,8,6,0,")
                                .append("0,0,6,7,0,8,2,0,0,")
                                .append("0,1,0,0,9,0,0,0,5,")
                                .append("0,0,0,0,0,1,0,0,8").toString())},
                {sudokuSolvingServiceImpl.stringRepresentationToBoardObject(
                        new StringBuilder()
                                .append("2,9,5,7,0,0,8,6,0,")
                                .append("0,3,1,8,6,5,0,2,0,")
                                .append("8,0,6,0,0,0,0,0,0,")
                                .append("0,0,7,0,5,0,0,0,6,")
                                .append("0,0,0,3,8,7,0,0,0,")
                                .append("5,0,0,0,1,6,7,0,0,")
                                .append("0,0,0,5,0,0,4,0,9,")
                                .append("0,2,0,6,0,0,3,5,0,")
                                .append("0,5,4,0,0,8,6,7,2").toString())},
                {sudokuSolvingServiceImpl.stringRepresentationToBoardObject(
                        new StringBuilder()
                                .append("0,2,3,4,5,6,7,8,0,")
                                .append("4,5,6,7,8,9,1,2,3,")
                                .append("7,8,9,1,2,3,4,5,6,")
                                .append("2,3,4,5,6,7,8,9,1,")
                                .append("5,6,7,8,9,1,2,3,4,")
                                .append("8,9,1,2,3,4,5,6,7,")
                                .append("3,4,5,6,7,8,9,1,2,")
                                .append("6,7,8,9,1,2,3,4,5,")
                                .append("0,1,2,3,4,5,6,7,0").toString())},
                {sudokuSolvingServiceImpl.stringRepresentationToBoardObject(
                        new StringBuilder()
                                .append("0,0,0,0,0,0,0,0,0,")
                                .append("0,0,0,0,0,0,0,0,0,")
                                .append("0,0,0,0,0,0,0,0,0,")
                                .append("0,0,0,0,0,0,0,0,0,")
                                .append("0,0,0,0,0,0,0,0,0,")
                                .append("0,0,0,0,0,0,0,0,0,")
                                .append("0,0,0,0,0,0,0,0,0,")
                                .append("0,0,0,0,0,0,0,0,0,")
                                .append("0,0,0,0,0,0,0,0,0").toString())}
        };

    }

}