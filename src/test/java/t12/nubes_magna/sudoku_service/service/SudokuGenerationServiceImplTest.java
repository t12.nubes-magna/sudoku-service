package t12.nubes_magna.sudoku_service.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import t12.nubes_magna.sudoku_service.model.Board;
import t12.nubes_magna.sudoku_service.model.Difficulty;
import t12.nubes_magna.sudoku_service.service.SudokuGenerationService;
import t12.nubes_magna.sudoku_service.service.SudokuSolvingService;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

@SpringBootTest
public class SudokuGenerationServiceImplTest extends AbstractTestNGSpringContextTests {

    @Autowired
    private SudokuGenerationService sudokuGenerationServiceImpl;
    @Autowired
    private SudokuSolvingService sudokuSolvingServiceImpl;

    @Test(dataProvider = "difficulties")
    public void testGenerateBoard(Difficulty difficulty) {
        final Board generatedBoard = sudokuGenerationServiceImpl.generateBoard(difficulty);
        final Board solvedBoard = sudokuSolvingServiceImpl.solve(generatedBoard);
        Assert.assertEquals(solvedBoard.isSolved(), true);
    }

    @DataProvider(name = "difficulties")
    private Object[][] prepareDifficultiesForTests() {
        final Object[][] difficulties = new Object[Difficulty.values().length][1];

        for (int row = 0; row < difficulties.length; ++row) {
            difficulties[row][0] = Difficulty.values()[row];
        }
        return difficulties;
    }
}